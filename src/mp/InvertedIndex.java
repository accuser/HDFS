package mp;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class InvertedIndex {

	public static class InvertIndexMapper extends  Mapper<Object,Text,Text,Text>{
		//存储单词和URI的组合
		private Text keyInfo = new Text();
		//存储词频
		private Text valueInfo = new Text();
		private FileSplit split;
		
		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			//获得<key,value>对所属的FileSplit对象
			split = (FileSplit)context.getInputSplit();
			StringTokenizer itr = new StringTokenizer(value.toString());
			while(itr.hasMoreTokens()){
				//key值由单词和URI组成,例如:MapReduce:1.txt
				//FileSplit.getPath()是获取目录
				keyInfo.set(itr.nextToken() + ":" + split.getPath().toString());
				//词频初始我1
				valueInfo.set("1");
				context.write(keyInfo, valueInfo);
			}
		}
	}
	
	public static class InvertedIndexCombiner extends Reducer<Text,Text,Text,Text>{
		private Text info = new Text();
		@Override
		protected void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			//统计词频
			int sum = 0;
			for(Text value :values){
				sum += Integer.parseInt(value.toString());
			}
			int splitIndex = key.toString().indexOf(":");
			//重新设置value值由URI和词频组成
			info.set(key.toString().substring(splitIndex + 1) + ":" + sum);
			//重新设置key值为单词
			key.set(key.toString().substring(0, splitIndex));
			context.write(key, info);
		}
		
	}
	
	public static class InvertedIndexReduce extends Reducer<Text,Text,Text,Text>{
		
		private Text result = new Text();
		@Override
		protected void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			//生成文档列表
			String fileList = new String();
			for(Text value : values){
				fileList += value.toString() + ";";
			}
			result.set(fileList);
			context.write(key, result);
		}
	}
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf,args).getRemainingArgs();
		if(otherArgs.length != 2){
			System.err.println("Usage:invertedindex<in><out>");
			System.exit(2);
		}
		
		Job job = new Job(conf, "InvertedIndex");
		job.setJarByClass(InvertedIndex.class);	//主类名
		job.setMapperClass(InvertIndexMapper.class);	//指定Mapper类
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setCombinerClass(InvertedIndexCombiner.class);	//指定Combiner函数
		job.setReducerClass(InvertedIndexReduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		System.exit(job.waitForCompletion(true)?0:1);

	}

}
