package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class DeleteFile {

	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		Path path = new Path("test-mv");
		boolean delete = hdfs.delete(path, false);
		//递归删除
		//boolean delete = hdfs.delete(path, false);
		System.out.println("delete?" + delete);
	}

}
