package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class CreateFile {

	/**
	 * 新建文件
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		byte[] buff = "hello world!".getBytes();
		FileSystem hdfs = FileSystem.get(conf);
		Path dfs = new Path("test");
		FSDataOutputStream out = hdfs.create(dfs);
		out.write(buff, 0, buff.length);
	}

}
