package hdfs;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.hadoop.fs.FsUrlStreamHandlerFactory;
import org.apache.hadoop.io.IOUtils;

public class Base {

	/**
	 * hadoop2.7.3中,org.apache.hadoop包的位置是：/share/hadoop/common中
	 */
	static final String PATH="hdfs://ns1/input";
	public static void main(String[] args) throws IOException {
		//hadoop fs -ls hdfs://hadoop:9000/
		URL.setURLStreamHandlerFactory(new FsUrlStreamHandlerFactory());
		final URL url = new URL(PATH);
		final InputStream in = url.openStream();
		/**
		 * IOUtils是hadoop提供的流的工具类
		 * in 标识输入流
		 * out 标识输出流
		 * buffsize 标识缓冲大小
		 * close 标识关闭流
		 */
		IOUtils.copyBytes(in, System.out, 1024,true);
	}
}
