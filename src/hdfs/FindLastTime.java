package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class FindLastTime {

	/**
	 * 查看文件最后修改时间
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		Path path = new Path("test-mv");
		FileStatus fileStatus = hdfs.getFileStatus(path);
		long modificationTime = fileStatus.getModificationTime();
		System.out.println("最后修改时间:"+modificationTime);
	}
}
