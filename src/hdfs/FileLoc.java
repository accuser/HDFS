package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class FileLoc {

	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		Path path = new Path("test-mv");
		FileStatus fileStatus = hdfs.getFileStatus(path);
		//getFileBlockLocations()可以查找制定文件在HDFS集群上的位置
		BlockLocation[] fileBlockLocations = hdfs.getFileBlockLocations(fileStatus, 0, fileStatus.getLen());
		int length = fileBlockLocations.length;
		for(int i=0;i<length;i++){
			String[] hosts = fileBlockLocations[i].getHosts();
			System.out.println("block:"+i+"location:"+hosts[i]);
		}
	}
}
