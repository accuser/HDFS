package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class RenameFile {

	/**
	 * 重命名
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(conf);
		Path old_path = new Path("test");
		Path new_path = new Path("test-mv");
		boolean rename = hdfs.rename(old_path, new_path);
		System.out.println("修改是否成功:"+rename);
	}
}
