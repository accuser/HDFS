package hdfs;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class FileBase {
	
	/**
	 * 源码位置:\hadoop-2.7.3\share\hadoop\common\sources
	 */
	static final String PATH="hdfs://ns1/input";
	public static void main(String[] args) throws IOException, URISyntaxException {
		FileSystem fileSystem = getFileSystem();
		list(fileSystem);
	}
	
	private static FileSystem  getFileSystem() throws IOException, URISyntaxException {
		return FileSystem.get(new URI(PATH), new Configuration());
	}
	
	private static void list(FileSystem fileSystem) throws IOException {
		FileStatus[] listStatus = fileSystem.listStatus(new Path("/"));
		for(FileStatus fileStatus  :listStatus){
			String isDir = fileStatus.isDirectory()?"文件夹":"文件";
			String permission = fileStatus.getPermission().toString();
			short replication = fileStatus.getReplication();
			long len = fileStatus.getLen();
			String path = fileStatus.getPath().toString();
			System.out.println(isDir+"\t"+permission+"\t"+replication+"\t"+len+"\t"+path);
		}
	}
	
	/*
	 * 上传本地文件到HDFS 
	 */
	
}
