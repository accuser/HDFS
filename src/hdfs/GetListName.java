package hdfs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.hdfs.protocol.DatanodeInfo;

public class GetListName {

	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		DistributedFileSystem hdfs = (DistributedFileSystem)fs;
		DatanodeInfo[] dataNodeStats = hdfs.getDataNodeStats();
		String[] names=new String[dataNodeStats.length];
		for(int i = 0;i<dataNodeStats.length;i++){
			//dataNodeStats[i].getHostName();可获取HDFS集群上所有的节点名称
			names[i] = dataNodeStats[i].getHostName();
			System.out.println("node" + i + "name" +names[i]);
		}
	}
}
